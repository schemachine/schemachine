package main

import (
	"os"
	"schemachine/generator"
	"schemachine/schema"
)

func main() {
	data, err := os.ReadFile(os.Args[1])

	if err != nil {
		panic(err)
	}

	group, err := schema.DecodeSchemaGroup(string(data))

	if err != nil {
		panic(err)
	}

	result, err := generator.Generate(group, generator.DefaultOptions)

	if err != nil {
		panic(err)
	}

	result.Save(os.Args[2])
}
