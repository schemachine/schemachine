package generator

import (
	"schemachine/schema"
	"sort"
)

func getSortedSchemaFields(definition schema.Schema) []string {
	keys := make([]string, len(definition.Fields))

	i := 0

	for key, _ := range definition.Fields {
		keys[i] = key
		i++
	}

	sort.Strings(keys)

	return keys
}
