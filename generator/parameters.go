package generator

import (
	"errors"
	"schemachine/schema"
)

func generateSQLParameters(definition schema.Schema, includeTypes bool) (string, error) {
	parameters := ""

	keys := getSortedSchemaFields(definition)

	for i, fieldName := range keys {
		fieldType := definition.Fields[fieldName]

		sqlType := ""

		if fieldType == "string" {
			sqlType = "text"
		} else if fieldType == "smallString" {
			sqlType = "varchar"
		} else if fieldType == "int" {
			sqlType = "integer"
		} else if fieldType == "timestamp" {
			sqlType = "timestamp"
		} else if fieldType == "jsonData" {
			sqlType = "text"
		} else {
			return "", errors.New("invalid field type: " + fieldType)
		}

		parameters += fieldName

		if includeTypes {
			parameters += " "
			parameters += sqlType

			if definition.Identifier == fieldName {
				parameters += " PRIMARY KEY"
			}
		}

		if i != len(definition.Fields)-1 {
			parameters += ", "
		}
	}

	return parameters, nil
}

func fieldTypeToGoType(fieldType string) string {
	if fieldType == "smallString" {
		return "string"
	}

	if fieldType == "timestamp" {
		return "time.Time"
	}

	if fieldType == "jsonData" {
		return "string"
	}

	return fieldType
}
