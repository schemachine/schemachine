package main

import (
	"example/database"
	"fmt"
	"time"
)

func main() {
	fmt.Println("=====", "Setting up the database...", "=====")

	db, err := database.OpenDatabase("sqlite3", "example.db")

	if err != nil {
		panic(err)
	}

	fmt.Println(db)

	fmt.Println("=====", "Creating users...", "=====")

	users := make([]database.User, 0)

	for i := 0; i < 10; i++ {
		username := fmt.Sprintf("user%d", i+1)

		user := database.User{
			Name:    username,
			Created: time.Now().UTC(),
			Metadata: database.SerializeJSON(map[string]interface{}{
				"description": nil,
			}),
		}

		users = append(users, user)

		fmt.Println(user)

		err = db.AddUser(user)

		if err != nil {
			panic(err)
		}
	}

	fmt.Println("=====", "Updating users...", "=====")

	for i := 0; i < 10; i++ {
		user := users[i]

		user.Metadata = database.SerializeJSON(map[string]interface{}{
			"description": "Hello, World! I am " + user.Name + ".",
		})

		err := db.UpdateUser(user)

		if err != nil {
			panic(err)
		}

		fmt.Println(user)
	}

	fmt.Println("=====", "Fetching users...", "=====")

	for i := 0; i < 10; i++ {
		username := fmt.Sprintf("user%d", i+1)

		user, err := db.GetUser(username)

		if err != nil {
			panic(err)
		}

		fmt.Println(user)
	}

	fmt.Println("=====", "Deleting users...", "=====")

	for i := 9; i > -1; i-- {
		username := fmt.Sprintf("user%d", i+1)

		db.DeleteUser(username)

		if err != nil {
			panic(err)
		}
	}

	err = db.Close()

	if err != nil {
		panic(err)
	}
}
