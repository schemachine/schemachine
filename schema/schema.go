package schema

import (
	"encoding/json"
	"strings"
)

type Schema struct {
	Name       string            `json:"name"`
	TableName  string            `json:"table_name"`
	Identifier string            `json:"identifier"`
	Fields     map[string]string `json:"fields"`
}

type SchemaGroup struct {
	Name             string   `json:"name"`
	Imports          []string `json:"imports"`
	DatabaseBackends []string `json:"database_backends"`
	Schemas          []Schema `json:"schemas"`
}

func DecodeSchemaGroup(data string) (SchemaGroup, error) {
	var group SchemaGroup

	err := json.Unmarshal([]byte(data), &group)

	return group, err
}

func SchemaNameToGoName(name string) string {
	return strings.ReplaceAll(strings.Title(strings.ReplaceAll(name, "_", " ")), " ", "")
}

// IsValidName() checks if a name is safe to use.
// This is because we use string concatination with the schema names to create database statements.
// So obviously if we don't do this it can easily lead to SQL injection.
// But it's also low-risk since an external attacker cannot control those names.

func IsValidName(text string) bool {
	for i := 0; i < len(text); i++ {
		switch text[i] {
		case 'a':
			continue
		case 'b':
			continue
		case 'c':
			continue
		case 'd':
			continue
		case 'e':
			continue
		case 'f':
			continue
		case 'g':
			continue
		case 'h':
			continue
		case 'i':
			continue
		case 'j':
			continue
		case 'k':
			continue
		case 'l':
			continue
		case 'm':
			continue
		case 'n':
			continue
		case 'o':
			continue
		case 'p':
			continue
		case 'q':
			continue
		case 'r':
			continue
		case 's':
			continue
		case 't':
			continue
		case 'u':
			continue
		case 'v':
			continue
		case 'w':
			continue
		case 'x':
			continue
		case 'y':
			continue
		case 'z':
			continue
		case '_':
			continue
		default:
			return false
		}
	}

	return true
}
